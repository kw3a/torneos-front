import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import Torneo from '../src/documents/Torneo';
import Navbar from './documents/NavBar';
import reportWebVitals from './reportWebVitals';
import Footer from './documents/Footer';

const root = ReactDOM.createRoot(document.getElementById('root'));
const partidas = [
  ["equipo A", "equipo B", 1, '12-10-2022'],
  ["equipo C", "equipo D", 1, '12-11-2022'],
  ["equipo E", "equipo F", 1, '11-10-2022'],
  ["equipo G", "equipo H", 1, '11-11-2022'],
  ["equipo I", "equipo J", 1, '11-12-2022'],
  ["equipo K", "equipo L", 1, '11-13-2022'],
  ["equipo M", "equipo N", 1, '11-14-2022'],
  ["equipo O", "equipo P", 1, '11-15-2022'],
  ["equipo Q", "equipo R", 2, '11-16-2022'],
  ["equipo S", "equipo T", 2, '11-17-2022'],
  ["equipo U", "equipo V", 2, '11-18-2022'],
  ["equipo W", "equipo X", 2, '11-19-2022'],
  ["equipo Y", "equipo Z", 3, '11-20-2022'],
  ["equipo AA", "equipo BB", 3, '11-21-2022'],
  ["equipo CC", "equipo DD", 4, '11-22-2022'],
];

export default partidas;

root.render(
  <React.StrictMode>
    <Navbar></Navbar>
    <Torneo partidas={partidas}></Torneo>
    <Footer></Footer>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
