import React from "react";
import '../style/partida.css'

const Partida = ({ equipoA, equipoB, bracket, fecha }) => {
  return (
    <div className="partida">
      <div className="equipo">{equipoA}</div>
      <div className="equipo">{equipoB}</div>
      <div className="bracket">Bracket: {bracket}</div>
      <div className="fecha">Fecha: {fecha}</div>
    </div>
  );
};

export default Partida;
