import React from "react";
import "../style/torneo.css"; // Importa el archivo de estilos CSS específico para el torneo

const Partida = ({ equipoA, equipoB, bracket, fecha }) => {
  return (
    <div className={`partida bracket-${bracket}`}>
      <div className="equipo equipo-a">{equipoA}</div>
      <div className="versus">vs</div>
      <div className="equipo equipo-b">{equipoB}</div>
      {/* <div className="bracket">Bracket: {bracket}</div> */}
      <div className="fecha">Fecha: {fecha}</div>
    </div>
  );
};

const Columna = ({ partidas }) => {
  return (
    <div className="columna">
      <h2>Bracket {partidas[0][2]}</h2>
      {partidas.map((partida, index) => (
        <Partida
          key={index}
          equipoA={partida[0]}
          equipoB={partida[1]}
          bracket={partida[2]}
          fecha={partida[3]}
        />
      ))}
    </div>
  );
};

const Torneo = ({ partidas }) => {
  return (
    <div className="torneo">
      <Columna partidas={partidas.slice(0, 8)} />
      <div className="conector" />
      <Columna partidas={partidas.slice(8, 12)} />
      <div className="conector" />
      <Columna partidas={partidas.slice(12, 14)} />
      <div className="conector" />
      <Columna partidas={partidas.slice(14, 15)} />
    </div>
  );
};

export default Torneo;
