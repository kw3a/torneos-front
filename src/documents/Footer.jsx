import React from "react";
import { FaFacebook, FaYoutube, FaDiscord, FaTwitter } from "react-icons/fa";
import "../style/footer.css";

const Footer = () => {
  return (
    <footer className="footer">
      <div className="social-icons">
        <span className="social-text">Nuestras redes sociales:</span>
        <div className="icon-container">
          <a href="https://www.facebook.com/" className="icon-link">
            <FaFacebook className="social-icon facebook-icon" />
          </a>
          <a href="https://www.youtube.com/" className="icon-link">
            <FaYoutube className="social-icon youtube-icon" />
          </a>
          <a href="https://discord.com/" className="icon-link">
            <FaDiscord className="social-icon discord-icon" />
          </a>
          <a href="https://twitter.com/" className="icon-link">
            <FaTwitter className="social-icon twitter-icon" />
          </a>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
