import React from "react";
import { FiSettings } from "react-icons/fi";
import { FaBars } from 'react-icons/fa';
import { useState } from 'react';
import "../style/NavBar.css";

const Navbar = () => {
  const [isDropdownOpen, setIsDropdownOpen] = useState(false);

  const handleDropdownToggle = () => {
    setIsDropdownOpen(!isDropdownOpen);
  };

  const handleOptionClick = (option) => {
    console.log('Opción seleccionada:', option.label);
  };

  return (
    <nav className="navbar">
      <div className="dropdown">
        <button className="dropdown-btn" onClick={handleDropdownToggle}>
          <FaBars /> {/* Ícono de tres líneas */}
        </button>
        {isDropdownOpen && (
          <div className="dropdown-content">
            <button
              className="dropdown-option"
              onClick={() => handleOptionClick({ label: 'Botón 1' })}
            >
              Dota 2
            </button>
            <button
              className="dropdown-option"
              onClick={() => handleOptionClick({ label: 'Botón 2' })}
            >
              League of legends
            </button>
          </div>
        )}
      </div>

      <div className="navbar-brand">
        <FiSettings className="navbar-icon" />
        <span className="navbar-name">GameRise</span>
      </div>
      <ul className="navbar-list">
        <li className="navbar-item">
          <a href="/" className="navbar-link">
            Inicio
          </a>
        </li>
        <li className="navbar-item">
          <a href="/about" className="navbar-link">
            Sobre nosotros
          </a>
        </li>
        <li className="navbar-item">
          <a href="/contact" className="navbar-link">
            Contáctanos
          </a>
        </li>
      </ul>
    </nav>
  );
};

export default Navbar;
